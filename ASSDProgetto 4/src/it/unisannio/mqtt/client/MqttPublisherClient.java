package it.unisannio.mqtt.client;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttPublisherClient extends WSNMqttClient {
	/**
	 * Costruttore per publisher MQTT
	 * @param clientId e' l'id del publisher
	 */
	public MqttPublisherClient(String clientId) {
		super(clientId);
	}
	
	/**
	 * Costruttore per publisher MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del publisher
	 */
	public MqttPublisherClient(String remoteBrokerIP, String clientId) {
		super(remoteBrokerIP, clientId);
	}
	
	/**
	 * Costruttore per publisher MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del publisher
	 * @param QoS e' la qualita' del servizio (0 per politica at most once, 1 per politica at least once, 2 per politica exactly once)
	 */
	public MqttPublisherClient(String remoteBrokerIP, String clientId, int QoS) {
		super(remoteBrokerIP, clientId, QoS);
	}

	/**
	 * Wrapper per il metodo publish di MQTT
	 * @param topic e' il nome del topic su cui effettuare la pubblicazione
	 * @param msg e' il messaggio da pubblicare
	 */
	public void publish(String topic, String msg) {
		try {
			if(client.isConnected()) {
				// Creazione del messaggio MQTT
				MqttMessage message = new MqttMessage();
				message.setQos(QoS); 
				message.setPayload(msg.getBytes());

				// Pubblica il messaggio sul broker
				client.publish(topic, message);
				System.out.println("Publishing message: " + msg);
				System.out.println("To topic " + topic + "\n");
				message.clearPayload();
			}
		} catch (MqttException e) { }
	}

	/**
	 * Wrapper per il metodo subscribe di MQTT
	 * @param topic e' il nome del topic a cui effettuare la sottoscrizione
	 * N.B.: Nel publisher non mi interessa implementarlo
	 */
	public void subscribe(String topic) { }
	
	/**
	 * Questa callback e' invocata quando un messaggio di un topic a cui siamo sottoscritti viene ricevuto. 
	 * Quindi non serve implementarla qui per il publisher
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception { }
}