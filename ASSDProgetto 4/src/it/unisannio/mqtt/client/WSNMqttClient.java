package it.unisannio.mqtt.client;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public abstract class WSNMqttClient implements MqttCallback {
	// Parametri di default 
	private String brokerProtocol = "tcp";
	private String brokerIP = "localhost";
	private String brokerPort = "1883";
	private String LOCAL_BROKER_URL = brokerProtocol + "://" + brokerIP + ":" + brokerPort;
	//protected static final String LOCAL_BROKER_URL = "tcp://localhost:1883";
	protected static final int QOS = 0;

	// Rappresenta un data store permanente, utilizzato per salvare i messaggi in-flight in uscita e in entrata,
	// consentendo la consegna alla QoS specificata. 
	// MemoryPersistence e' un'implementazione di MqttClientPersistence questa interfaccia.
	protected MemoryPersistence persistence = null;
	protected MqttClient client;
	protected String remoteBrokerUrl;
	protected String clientId;
	protected int QoS;

	/**
	 * Costruttore per client MQTT, il clientId viene generato automaticamente
	 */
	public WSNMqttClient() {
		super();
		this.remoteBrokerUrl = LOCAL_BROKER_URL;
		this.clientId = MqttClient.generateClientId();
		this.QoS = QOS;
		initAndConnect();
	}
	
	
	/**
	 * Costruttore per client MQTT
	 * @param clientId e' l'id del client
	 */
	public WSNMqttClient(String clientId) {
		super();
		this.remoteBrokerUrl = LOCAL_BROKER_URL;
		this.clientId = clientId;
		this.QoS = QOS;
		initAndConnect();
	}

	/**
	 * Costruttore per client MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del client
	 */
	public WSNMqttClient(String remoteBrokerIP, String clientId) {
		this.remoteBrokerUrl = brokerProtocol + "://" + remoteBrokerIP + ":" + brokerPort;
		this.clientId = clientId;
		this.QoS = QOS;
		initAndConnect();
	}

	/**
	 * Costruttore per client MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del client
	 * @param QoS e' la qualita' del servizio (0 per politica at most once, 1 per politica at least once, 2 per politica exactly once)
	 */
	public WSNMqttClient(String remoteBrokerIP, String clientId, int QoS) {
		this.remoteBrokerUrl = brokerProtocol + "://" + remoteBrokerIP + ":" + brokerPort;
		this.clientId = clientId;
		this.QoS = QoS;
		if(QoS > 0) persistence = new MemoryPersistence();
		initAndConnect();
	}

	/**
	 * Questo metodo crea un client MQTT e lo connette al broker
	 * @throws MqttException, quando viene lanciata questa eccezione viene catturata dalla callback connectionLost(Throwable t) 
	 */
	public void initAndConnect() {
		try {
			// Creazione client, il costruttore MqttClient(String, String, MqttClientPersistence) e' usato per mantenere i messaggi QoS 1 e 2.
			// Se MqttClientPersistence e' null si assume QoS = 0 e non si conserveranno i messaggi
			client = new MqttClient(remoteBrokerUrl, clientId, persistence);
			// La callback e' messageArrived (non serve implementarla lato publisher)
			client.setCallback(this);

			// Connessione al broker (ActiveMQ)
			client.connect();
			System.out.println("Connected to " + remoteBrokerUrl);
		} catch (MqttException e) { System.out.println(e.getMessage()); } // Quando viene avviato il client ma il server non e' stato ancora avviato viene semplicemente stampato un messaggio di errore (per ritentare la connessione si potrebbe aggiungere una chiamata a connectionLost()
	}
	
	/**
	 * Wrapper per il metodo publish di MQTT
	 * @param topic e' il nome del topic su cui effettuare la pubblicazione
	 * @param msg e' il messaggio da pubblicare
	 */
	public abstract void publish(String topic, String msg);

	/**
	 * Wrapper per il metodo subscribe di MQTT
	 * @param topic e' il nome del topic a cui effettuare la sottoscrizione
	 */
	public abstract void subscribe(String topic);

	/**
	 * Questa callback e' invocata quando si perde la connesione (quindi client e server hanno gia' stabilito una connessione in precedenza).
	 */
	public void connectionLost(Throwable t) {
		t.printStackTrace();
		while(!client.isConnected()) {
			try {
				System.out.println(t.getMessage() + "\nTentativo di riconnessione\n");
				client.connect();
			} catch (MqttException e) { e.printStackTrace(); }
		}
	}

	/**
	 * Questa callback e' invocata quando un messaggio e' pubblicato da questo client. 
	 */
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		//Non lo implemento perche' ho settato QoS a 0 di default e per ora va bene cosi'
	}

	/**
	 * Questa callback e' invocata quando un messaggio di un topic a cui siamo sottoscritti viene ricevuto.
	 */
	public abstract void messageArrived(String topic, MqttMessage message) throws Exception;

}