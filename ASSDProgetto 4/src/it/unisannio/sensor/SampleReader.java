package it.unisannio.sensor;

import java.io.IOException;

import org.iot.raspberry.grovepi.GrovePi;
import org.iot.raspberry.grovepi.devices.GroveTemperatureAndHumiditySensor;
import org.iot.raspberry.grovepi.pi4j.GrovePi4J;

public class SampleReader {
	public double getSample() throws IOException {
		GrovePi grovePi = new GrovePi4J();
		GroveTemperatureAndHumiditySensor dht = new GroveTemperatureAndHumiditySensor(grovePi, 4, GroveTemperatureAndHumiditySensor.Type.DHT11);
		return dht.get().getHumidity();
	}
}
