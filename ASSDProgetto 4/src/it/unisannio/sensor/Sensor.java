package it.unisannio.sensor;

import it.unisannio.mqtt.client.MqttPublisherClient;
import it.unisannio.mqtt.client.MqttSubscriberClient;

public abstract class Sensor extends Thread {
	private static final String REGISTRATION_TOPIC = "registration";
	private static final String AWAIT_TOPIC = "await";
	static MqttPublisherClient publisher;
	static MqttSubscriberClient subscriber;
	String type;
	String sensorId;
	String url;
	public Sensor(String type, String sensorId) {
		this.type = type;
		this.sensorId = sensorId;
		
		subscriber = new MqttSubscriberClient(sensorId);
		publisher = new MqttPublisherClient(sensorId + "Publisher");
		
		registerAndWait(sensorId, type);
	}
	
	public Sensor(String url, String type, String sensorId) {
		this.type = type;
		this.sensorId = sensorId;
		
		subscriber = new MqttSubscriberClient(url, sensorId);
		publisher = new MqttPublisherClient(url, sensorId + "Publisher");
		
		registerAndWait(sensorId, type);
	}
	
	private static void registerAndWait(String sensorId, String type) {
		String registrationMessage = "{" +
			       "\"sensorId\" : \"" + sensorId + "\"," +
			       "\"type\"  : \"" + type + "\"" +	     
		       "}";
		subscriber.subscribe(Sensor.AWAIT_TOPIC);
		publisher.publish(Sensor.REGISTRATION_TOPIC, registrationMessage);
	}
	
	/**
	 * Questo metodo viene eseguito quando viene avviato il thread, 
	 * ogni specializzazione del sensore deve implementare la propria versione
	 */
	public abstract void run();
}	