package it.unisannio.sensor;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import it.unisannio.mqtt.client.MqttMessageListener;

public class SensorImpl extends Sensor {
	SampleReader reader = null;
	public SensorImpl(String type, String sensorId) { super(type, sensorId); }
	public SensorImpl(String url, String type, String sensorId, SampleReader reader) { 
		super(url, type, sensorId); 
		this.reader = reader;
	}

	@Override
	public void run() {
		// Imposto un listener per il topic di registrazione 
		subscriber.setMessageListener(new MqttMessageListener() {
			public void messageReceived(String message) {
				try {
					JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject) parser.parse(message);
					String rcvdSensorId = (String) json.get("sensorId");
					String rcvdTopic = (String) json.get("topic");
					// Se mi arriva un messaggio con il mio ID inizio a pubblicare campioni sul topic ricevuto
					if(sensorId.equals(rcvdSensorId)) {
						publishSamples(rcvdTopic);
					}
				} catch (ParseException e) { e.printStackTrace(); }
			}
		});
	}
	// N.B: Se viene richiamato questo metodo da SensorLocalTest si ha un eccezione in quanto la variabile SampleReader non � inizializzata
	public void publishSamples(final String topic) {
		String msgToPublish;
		if(reader != null) { // Se si usa un sensore reale
			while(true) {
				try {
					double sample = reader.getSample();
					System.out.println(sample);
					String payload = String.valueOf(sample);
					msgToPublish = "{" +
						       "\"sensorId\" : \"" + sensorId + "\"," +
						       "\"sample\"  : \"" + payload + "\"" +	     
					       "}";
					publisher.publish(topic, msgToPublish);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {// Se si emula il sensore
			double[] samples = new double[] {10, 10, 8, 8, 4, 3, 2, 1, 3, 6, 8, 10, 10, 12, 15, 20, 22, 23, 24, 25};
			int i = 0;
			try {	
				while(true) {
					String payload = String.valueOf(samples[i++ % samples.length]);
					msgToPublish = "{" +
						       "\"sensorId\" : \"" + sensorId + "\"," +
						       "\"sample\"  : \"" + payload + "\"" +	     
					       "}";
					publisher.publish(topic, msgToPublish);
					// Emulazione di sampling del sensore con periodo di sampling di 1000 ms
					sleep(1000);
				}
			} catch (InterruptedException e) { e.printStackTrace();	}
		}
	}
}
