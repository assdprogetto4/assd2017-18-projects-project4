package it.unisannio.sensor;

public class SensorRunner {
	// Avvia il sensore che comunica con un broker remoto
	public static void main(String[] args) {
		String remoteBrokerURL = "172.20.10.4";
		SampleReader reader = new SampleReader();
		new SensorImpl(remoteBrokerURL, SensorType.HUMIDITY, SensorType.HUMIDITY+1, reader).start();
	}
}
