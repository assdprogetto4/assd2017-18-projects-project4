package it.unisannio.sensor;

public class SensorType {
	public static final String TEMPERATURE = "temperature";
	public static final String HUMIDITY = "humidity";
	public static final String BRIGHTNESS = "brightness";
}
