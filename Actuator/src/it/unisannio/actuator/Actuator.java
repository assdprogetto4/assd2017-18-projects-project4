package it.unisannio.actuator;

interface Action{
	public String getActionName();
	public void go();
}

/**
 * Ci sono 3 stati per l'attuatore: 
 * - on 
 * - off 
 * - stopped
 * Inizialmente ci troviamo nello stato stopped e passiamo da stopped a off.
 * Passiamo da off a on quando viene richiamata actuate (e ci restiamo ogni volta che viene richiamata actuate)
 * Passiamo da on a off quando scatta il timeout per la ricezione dei messaggi (2 secondi mi pare)
 * Quindi facendo un automa a stati finiti abbiamo 3 stati e le transizioni sono actuate
 *
 * Qui si trova la logica di attuazione usata sia in LTActuatorNode che in HTActuatorNode
 */
public class Actuator extends Thread {
	private static enum States {on, off};
	private States aState = States.off;
	private Action action;
	
	public Actuator(Action a) { action = a; }
	
	public void run() {
		try {synchronized(this) {wait();}}catch(InterruptedException e) {}
		while(true) {
			try {
				if(aState == States.off) {
					System.out.println("Attiva " + action.getActionName());
					action.go();
				}
				aState = States.on;
				sleep(1500); 
				// Se non arriva niente in 1500 ms vado nello stato off
				synchronized(this) {
					aState = States.off;
					System.out.println("Spegni " + action.getActionName());
					wait();
				}
			}catch(InterruptedException e) {
				// E' usato per bypassare il cambio di stato (da on a off)
				// quando ho un actuate che mi sveglia il thread quando si trova in sleep.
				// In questo modo se sto in sleep e viene invocata actuate (quindi eseguite interrupt e notify)
				// non passo da on a off (non entro nel blocco synchronized) 
				// ma arrivo qui, non faccio niente e rifaccio un giro nel while
			}
		}
	}
	
	public synchronized void actuate() {
		interrupt();
		notify();
	}
}
