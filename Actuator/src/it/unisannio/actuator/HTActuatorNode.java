package it.unisannio.actuator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

import org.apache.activemq.ActiveMQConnectionFactory;

public class HTActuatorNode {
	public static void main(String[] args) {
		String url = "tcp://localhost:61616";
		TopicConnectionFactory connFactory = new ActiveMQConnectionFactory(url);
		try {
			TopicConnection connection = connFactory.createTopicConnection();
			TopicSession session = connection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			
			Topic temperatureTopic = session.createTopic("temperature");
			String selectorHT = "temperature >=20";
			TopicSubscriber subscriber = session.createSubscriber(temperatureTopic,selectorHT, true);
			
			Actuator hta = new Actuator(new Action() {
				public String getActionName() {
					return "Irrigazione";
				}
				public void go() {
					/* Qui viene effettuata l'azione da fare con l'attuatore.
					 * Il corpo del metodo e' vuoto perche' non ho l'attuatore,
					 * nel caso avessi l'hardware qui implemento le azioni da fare
					 */
				}
			});
			hta.start();
			/* Una volta avviate le procedure di irrigazione per interromperle devo vedere quando non arrivano piu' messaggi
			 * quindi se non arrivano + campioni dopo 2 secondi (per la traccia) e' evidente che t<20
			 * quindi posso ragionare sulla base del periodo di campionamento
			 */
			subscriber.setMessageListener(new MessageListener() {
				public void onMessage(Message msg) { // Quando ricevo il messaggio estraggo il valore della temperatura (>=20 gradi di sicuro quando viene eseguito il listener)
					try {
						System.out.println(msg.getDoubleProperty("temperature"));
						hta.actuate(); // Invoco actuate che fa quello che serve per la procedura di irrigazione
					}catch(JMSException e) { System.err.println("Error " + e); }
				}
			});
			connection.start();
		}catch(JMSException e) {
			System.err.println("Error: "+e);
		}
	}
}
