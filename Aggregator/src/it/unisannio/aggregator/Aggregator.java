package it.unisannio.aggregator;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import org.apache.activemq.ActiveMQConnectionFactory;

import it.unisannio.mqtt.client.MqttSubscriberClient;

public abstract class Aggregator extends Thread {
	protected MqttSubscriberClient subscriber;
	protected TopicPublisher publisher;
	protected TopicConnectionFactory connFactory;
	protected TopicConnection connection;
	protected TopicSession session;
	protected String operation;
	
	// Topic per gli attuatori a cui fa riferimento il publisher (JMS)
	private Topic publisherTopic;
	public String url, pubTopic, subTopic, subId;	

	public Aggregator(String url, String pubTopic, String subTopic, String operation) {
		this.url = url;
		this.pubTopic = pubTopic;
		this.subTopic = subTopic;
		this.operation = operation;
	}
	
	public Aggregator(String url, String pubTopic, String subTopic, String subId, String operation) {
		this.url = url;
		this.pubTopic = pubTopic;
		this.subTopic = subTopic;
		this.subId = subId;
		this.operation = operation;
	}

	/**
	 * Inizializzazione del nodo aggregatore
	 */
	public void init() {
		try {
			connFactory = new ActiveMQConnectionFactory(url);
			connection = connFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

			publisherTopic = session.createTopic(pubTopic);
			publisher = session.createPublisher(publisherTopic);

			// Topic per i sensori a cui fa riferimento il subscriber (MQTT)
			subscriber = new MqttSubscriberClient();
			subscriber.subscribe(subTopic);
			connection.start();
		} catch(JMSException e) {
			System.out.println("Il broker JMS non e' raggiungibile");
		}
	}	

	/**
	 * Questo metodo viene eseguito quando viene avviato il thread, 
	 * ogni specializzazione dell'aggregatore deve implementare la propria versione
	 */
	public abstract void run();
}
