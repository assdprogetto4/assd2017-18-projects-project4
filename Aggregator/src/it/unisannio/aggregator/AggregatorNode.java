package it.unisannio.aggregator;

import java.util.ArrayList;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import it.unisannio.aggregator.operations.AggregatorOperation;
import it.unisannio.mqtt.client.MqttMessageListener;

public class AggregatorNode extends Aggregator {
	public AggregatorNode(String url, String pubTopic, String subTopic, String subId, String operation) {
		super(url, pubTopic, subTopic, subId, operation);
	}
	public AggregatorNode(String url, String pubTopic, String subTopic, String operation) {
		super(url, pubTopic, subTopic, operation);
	}

	@Override
	public void run() {
		// Settaggio del listener della classe MqttSubscriberClient
		subscriber.setMessageListener(new MqttMessageListener() {
			long timer;
			long timeout = 999;
			ArrayList<Double> samples = new ArrayList<Double>();
			ArrayList<String> producers = new ArrayList<String>();
			@Override
			public void messageReceived(String message) {
				try {
					double result = 0;

					// Al primo messaggio fa partire il timer
					if(samples.size() == 0) timer = System.currentTimeMillis();

					// Finche' non raggiungo il timeout aggiorno il numero di campioni ricevuti in una lista
					if((System.currentTimeMillis() - timer) < timeout) { 
						try {
							JSONParser parser = new JSONParser();
							JSONObject json = (JSONObject) parser.parse(message);
							String sensorId = (String) json.get("sensorId");
							String sample = (String) json.get("sample");
							// Mi assicuro di non ricevere pi� messaggi dallo stesso sensore nella finestra temporale stabilita
							if(!producers.contains(sensorId)) {
								producers.add(sensorId);
								samples.add(Double.parseDouble(sample));
							}
						} catch (ParseException e) { e.printStackTrace(); }
					}
					else { 
						// Quando raggiungo il timeout recupero la classe dell'operazione di aggregazione da effettuare
						Class<?> type = Class.forName("it.unisannio.aggregator.operations." + operation);
						AggregatorOperation opToDo = (AggregatorOperation) type.newInstance();
						// Recupero il risultato dell'operazione di aggregazione dei campioni
						result = opToDo.aggregate(samples);

						System.out.println("End timeout\nSamples received: " + samples + "\n" + type.getSimpleName() + ": " + result);

						TextMessage msgToActuators = session.createTextMessage();
						msgToActuators.setDoubleProperty("sample", result);
						System.out.println("Publishing to " + pubTopic + "\n");
						// Invio agli attuatori
						publisher.publish(msgToActuators);

						// Resetto la lista di producer e il numero di messaggi ricevuti
						samples.clear();
						producers.clear();
					}
				} catch(JMSException e) { System.err.println("Error " + e); 
				} catch (ClassNotFoundException e) {
					System.err.println("Classe non trovata: " + e.getMessage());
				} catch (InstantiationException e) {
					System.err.println("Eccezione istanziazione: " + e.getMessage());
				} catch (IllegalAccessException e) {
					System.err.println("IllegalAccessException: "  + e.getMessage());
				}
			}
		});
	}
}
