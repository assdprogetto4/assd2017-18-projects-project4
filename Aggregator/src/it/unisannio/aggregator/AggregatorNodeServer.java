package it.unisannio.aggregator;

import it.unisannio.udp.heartbeat.Ping;
import it.unisannio.udp.heartbeat.UDPFaultDetector;

public class AggregatorNodeServer {
	private String brokerProtocol = "tcp";
	private String brokerIP = "localhost";
	private String brokerPort = "61616";
	private String LOCAL_BROKER_URL = brokerProtocol + "://" + brokerIP + ":" + brokerPort;
	private String url;
	private int port;
	private UDPFaultDetector fd;
	private Ping ping;
	private AggregatorNode aggregator;
	private AggregatorNode backupAggregator;
	
	public AggregatorNodeServer(String subTopic, String pubTopic, String subId, String operation, int port) {
		this.port = port;
		this.url = LOCAL_BROKER_URL;
		aggregator = new AggregatorNode(url, pubTopic, subTopic, subId, operation);
		backupAggregator = new AggregatorNode(url, pubTopic, subTopic, subId, operation);
		init();
	}
	
	public AggregatorNodeServer(String subTopic, String pubTopic, String operation, int port) {
		this.port = port;
		this.url = LOCAL_BROKER_URL;
		aggregator = new AggregatorNode(url, pubTopic, subTopic, operation);
		backupAggregator = new AggregatorNode(url, pubTopic, subTopic, operation);
		init();
	}
	
	private void init() {
		// creazione fault detector e associazione all'aggregatore di backup
		fd = new UDPFaultDetector(port, backupAggregator);
		
		// creazione ping per il nodo aggregatore principale
		ping = new Ping("127.0.0.1", port);
		aggregator.init();
	}
	
	public void start() {
		ping.start();
		aggregator.start();
		fd.start();
	}
}
