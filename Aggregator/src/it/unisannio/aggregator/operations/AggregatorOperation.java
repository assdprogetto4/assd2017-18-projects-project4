package it.unisannio.aggregator.operations;

import java.util.Collection;

public interface AggregatorOperation {
	public double aggregate(Collection<Double> collection);
}
