package it.unisannio.aggregator.operations;

import java.util.Collection;

public class Max implements AggregatorOperation {
	public double aggregate(Collection<Double> collection) {
		double max = Double.MIN_VALUE;
		for(Double item : collection) if(item > max) max = item;
		return max;
	}
}
