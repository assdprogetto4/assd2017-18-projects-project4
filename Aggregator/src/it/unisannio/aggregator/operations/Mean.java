package it.unisannio.aggregator.operations;

import java.util.Collection;

public class Mean implements AggregatorOperation {
	public double aggregate(Collection<Double> collection) {
		int samples = collection.size();
		double tot = 0;
		for(Double item : collection) tot += item; 
		return tot/samples;
	}
}
