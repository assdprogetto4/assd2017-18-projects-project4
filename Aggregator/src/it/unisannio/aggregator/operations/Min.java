package it.unisannio.aggregator.operations;

import java.util.Collection;

public class Min implements AggregatorOperation {
	public double aggregate(Collection<Double> collection) {
		double min = Double.MAX_VALUE;
		for(Double item : collection) if(item < min) min = item;
		return min;
	}
}
