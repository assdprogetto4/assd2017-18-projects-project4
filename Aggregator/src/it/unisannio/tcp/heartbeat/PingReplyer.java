package it.unisannio.tcp.heartbeat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Deve essere interrogabile dall'istanza di backup periodicamente
 */
public class PingReplyer extends TCPHeartBeat {

	public PingReplyer(int port) {	this.port = port; }

	/**
	 * Risponde ai ping con true
	 */
	public void run() {
		try {
			// Crea	la socket welcoming	alla porta specificata
			replyer = new ServerSocket(port);
			while(true)
			{
				Socket connectionSocket = replyer.accept();

				DataInputStream inFromBackupNode = new DataInputStream(connectionSocket.getInputStream());
				DataOutputStream outToBackupNode = new DataOutputStream(connectionSocket.getOutputStream());

				inFromBackupNode.readInt();
				outToBackupNode.writeBoolean(true); // Risponde al ping del nodo di backup

				connectionSocket.close();
			}
		} catch(IOException e) { e.printStackTrace(); } // Se il processo o la macchina su cui il processo e' in esecuzione va giu' non rispondera' con true ma sollevera' una IOException
	}
}
