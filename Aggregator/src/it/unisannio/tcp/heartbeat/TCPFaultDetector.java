package it.unisannio.tcp.heartbeat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import it.unisannio.aggregator.Aggregator;

/**
 * Questa classe istanziata all'interno del processo di backup dovr� attivare un thread 
 * il cui scopo � verificare con periodicit� che l'istanza principale stia funzionado ancora
 */
public class TCPFaultDetector extends TCPHeartBeat {
	private static final int isAlivePeriod = 100; // ms
	private Aggregator aggregator;

	public TCPFaultDetector(String iPAddress, int port, Aggregator aggregator) {
		this.iPAddress = iPAddress;
		this.port = port;
		this.aggregator = aggregator;
	}

	/**
	 * Invia ping al nodo master, se non arriva nessuna risposta subentra il nodo di backup
	 */
	public void run() {
		boolean isAlive = false;
		System.out.println("Failure detector started");
		try {
			do {
				requestor = new Socket(iPAddress, port);

				DataOutputStream outToMaster = new DataOutputStream(requestor.getOutputStream());
				DataInputStream inFromMaster = new DataInputStream(requestor.getInputStream());

				outToMaster.writeInt(1);
				isAlive = inFromMaster.readBoolean();

				requestor.close();
				sleep(isAlivePeriod);
			} while(isAlive);
		} catch (IOException e) { 
			System.out.println("Fault del nodo master rilevato, attivazione del nodo di backup");
			aggregator.init();
			aggregator.start();
		} catch (InterruptedException e) { e.printStackTrace(); }
	}
}
