package it.unisannio.tcp.heartbeat;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Le implementazioni TCPFaultDetector e PingReplyer realizzano l'heartbeat dal nodo di backup a quello master 
 */
public abstract class TCPHeartBeat extends Thread {
	protected static Socket requestor;
	protected String iPAddress;
	protected int port;
	protected static ServerSocket replyer;
	
	public abstract void run();
}
