package it.unisannio.udp.heartbeat;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ping extends UDPHeartBeat {
	private InetAddress IPAddress;

	public Ping(String IPAddress, int port) {
		try {
			this.IPAddress = InetAddress.getByName(IPAddress);
			this.port = port;
		} catch (UnknownHostException e) { e.printStackTrace(); }
	}

	/**
	 * Invia ping al nodo di backup (I'm alive) ogni isAlivePeriod/2
	 */
	public void run() {
		System.out.println("Ping started.");
		try {
			socket = new DatagramSocket();
			while(true) {
				ByteArrayOutputStream streamToBuffer = new ByteArrayOutputStream();
				DataOutputStream outToBackupNode = new DataOutputStream(streamToBuffer);

				String message = "I'm alive";

				outToBackupNode.write(message.getBytes());
				sendData = streamToBuffer.toByteArray();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port); 
								
				socket.send(sendPacket);
				
				sleep(isAlivePeriod/2);
			}
		} catch (IOException e) { e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
