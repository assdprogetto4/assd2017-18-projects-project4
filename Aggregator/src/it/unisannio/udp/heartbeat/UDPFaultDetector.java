package it.unisannio.udp.heartbeat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;

import it.unisannio.aggregator.Aggregator;

public class UDPFaultDetector extends UDPHeartBeat {
	private Aggregator aggregator;

	public UDPFaultDetector(int port, Aggregator aggregator) {	
		this.port = port;
		this.aggregator = aggregator;
	}

	/**
	 * Riceve ping dal nodo master, se non arriva nessuna risposta entro il timeout subentra il nodo di backup
	 */
	public void run() {
		try {
			System.out.println("Backup aggregator node started.");
			socket = new DatagramSocket(port);
			socket.setSoTimeout(isAlivePeriod);
			while(true)	{
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length); 
				socket.receive(receivePacket); 
			}
		} catch (SocketTimeoutException e) { 
			System.out.println("Fault del nodo master rilevato, attivazione del nodo di backup");
			aggregator.init();
			aggregator.start();
		} catch (IOException e) { e.printStackTrace(); }
	}
}
