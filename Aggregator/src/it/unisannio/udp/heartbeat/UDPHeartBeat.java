package it.unisannio.udp.heartbeat;

import java.net.DatagramSocket;

/**
 * Le implementazioni Ping e UDPFaultDetector realizzano l'heartbeat dal nodo master a quello di backup
 */
public abstract class UDPHeartBeat extends Thread {
	protected static final int isAlivePeriod = 100; // ms
	protected static DatagramSocket socket;
	protected int port;
	protected byte[] receiveData = new byte[1024];
	protected byte[] sendData; // = new byte[1024];

	public abstract void run();
}
