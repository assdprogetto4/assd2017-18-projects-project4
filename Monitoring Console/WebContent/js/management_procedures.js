/**
 * Apre la pagina di monitoraggio
 * @param element è la riga della tabella in cui si trova il bottone che richiama questa funzione
 * @returns
 */
function openMonitor(element) { 
	var row = element.parentNode.parentNode; 	// Recupero la riga della tabella in cui si trova il bottone
	var Cells = row.getElementsByTagName("td");	// Recupero le celle della riga
	var operation = Cells[2].innerText;			// Recupero il nome dell'operazione che si trova nella riga del bottone
	var topic = Cells[3].innerText				// Recupero il nome del topic che si trova nella riga del bottone
	console.log("Operation: " + operation + "\nTopic: " + topic);
	localStorage.setItem("operation", operation);// e li salvo nello storage locale del browser (così evito la servlet solo per propagarlo)
	localStorage.setItem("topic", topic);
	window.open("monitor.html")					// Apro la console di monitoraggio in una nuova finestra
}

/**
 * inizializza la lista dei topic recuperandola dal file di testo
 * @returns
 */
function initTopicsDropdownList() {
	jQuery.get("topic_dropdown_list.txt", function(data) {
		$("#topic_select").append(data);
	});
}

/**
 * inizializza la tabella dei topic (con le funzioni di aggregazione) recuperandola da file
 * @returns
 */
function initTopicsTable() {
	jQuery.get("topic_table.txt", function(data) {
		$("#topic_list tbody").append(data);
	});
}

/**
 * Aggiorna la lista di topic
 * @param id è l'id del tag select a cui aggiungere una nuova riga
 * @param value è la riga da aggiungere
 * @returns
 */
function updateDropdownList(id, value) {
    var select, i, option;
    select = document.getElementById(id);
    option = document.createElement('option');
    option.value = option.text = value;
    if($("#" + id + " option[value=" + value + "]").length == 0){
    	 select.add(option);
    	 console.log("lista aggiornata.");
	}
   
}

/**
 * Aggiunge il topic alla tabella e fa partire l'aggregatore associato all'operazione
 * @param submitForm è il form in cui viene specificato il topic e l'operazione di aggregazione da effettuare
 * @returns
 */
function addTopicAndStartAggregator(submitForm) {
	console.log("AAAAAAAAAAAAAA")
	// Recupera i parametri dalla pagina
	var $form 	= $(submitForm),
	topic 		= $form.find("select[name='topic']").val(),
	operation	= $form.find("select[name='operation']").val(),
	url 		= $form.attr("action");

	var exist = verifyTopic(topic, operation);
	if(exist) 
		swal("Il topic già esiste", 
			 "L'operazione " + operation + " sul topic " + topic + " è già stata creata",
			 "error");
	else {
		// Invia i dati alla servlet con il metodo post
		var posting = $.post(url,
			{ 
				topic: topic,
				operation: operation
			});
		// Aggiunge il risultato alla tabella dei topic
		posting.done(function(data) {
			swal("Topic Aggiunto!", 
				 "L'operazione " + operation + " sul topic " + topic + " è stata creata con successo",
				 "success");
			$("#topic_list tbody").append(data).html();
			var found = $('#sensor_topic', data);
			var value = found[0].innerText;
			updateDropdownList('topic_select', value);
		});
	}
}

/**
 * Avvia un sensore (o meglio gli dice che può iniziare a pubblicare campioni)
 * @returns
 */
function startSensor() {
	var sensorId = $('#sensor_select').find(":selected").text();
	var pubTopic = $('#topic_select').find(":selected").text();
	$('#sensor_select').find(":selected").attr("disabled", true);
	var subTopic = "await";
	var message = '{' +							// costruzione del messaggio in formato JSON
	       '"sensorId" : "' + sensorId + '",' +	// è l'id del sensore da avviare
	       '"topic"  : "' + pubTopic + '"' +	// è il nome del topic su cui pubblicare i campioni
	       '}';
	publish(subTopic, message);
	console.log("Sensore aggiunto.");
	swal("Sensore aggiunto.");
}

function publish(topic, message) {
	if(window.WebSocket) {
		var client;
		var login = '';
		var passcode = '';	
		var url = 'ws://localhost:61614';			// URI del broker ActiveMQ
		var destination = '/topic/' + topic;		// Costruzione seconda parte dell'URI
		
		console.log("Sending: " + message + "\nto " + destination + ".");
		
		// Callback per il metodo connect, effettua la connessione al broker e la pubblicazione sul topic
		var connect_callback = function(frame) {
			// Visualizza popup informativo
			console.log("Connessione effettuata!");
			// Effettua la subblicazione sul topic
			client.send(destination, {}, message);
		}

		// Callback per il metodo connect, in caso di errori di connessioni mostra un messaggio a video e nella console del browser
		var error_callback = function(error) {
			swal("Impossibile connettersi al broker MQTT", error, "error");
		}
		
		// Creazione client STOMP su websocket
		client = Stomp.client(url);
		// Connessione del client al broker
		client.connect(login, passcode, connect_callback, error_callback);
	} else {
		// Visualizza un messaggio se il browser non suporta websocket
		swal("Get a new Web Browser!", "Il tuo browser non supporta WebSockets", "error")
	}
}

/**
 * Resetta la lista dei sensori quando si cambia topic
 * @returns
 */
function resetSensorList() {
	// resetta la lista
	$('#sensor_select')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="" disabled selected>Seleziona un sensore</option>')
	    .val('');
}

/**
 * Inizializza la lista sensori quando si seleziona un topic
 * @param sensorType è il topic (tipo di sensore: umidità, temperatura o luminosità) 
 * @returns
 */
function initSensorsDropdownList(sensorType) {
	resetSensorList();
	var url = "/Administration_Console/ManagementConsole";
	// Invia i dati alla servlet con il metodo get
	var getting = $.get(url, { type: sensorType.value });
	// Aggiunge il risultato alla tabella dei topic
	getting.done(function(data) {
		console.log("Done.");
		$("#sensor_select").append(data).html();
	}).fail(function() {
	    console.log("Error");
	}).always(function() {
		console.log("Finished");
	});
}

/**
 * Verifica se l'operazione sul topic è già stata creata
 * @param topic è il nome del topic
 * @param operation è il nome dell'operazione
 * @returns true se l'operazione sul topic è già stata creata, false altrimenti
 */
function verifyTopic(topic, operation) {
	var tableBody = document.getElementById('topic_list').children[1];
	var numRows = tableBody.children.length;
	var tableRow, rowTopic, rowOperation;
	
	for(var i = 0; i < numRows; i++) {
		tableRow = tableBody.children[i];
		rowOperation = tableRow.children[2].innerText;
		rowTopic = tableRow.children[3].innerText;
		
		if((operation == rowOperation) && (topic == rowTopic)) return true;
	}
	return false;
}

