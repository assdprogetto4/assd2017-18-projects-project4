/**
 * Effettua la sottoscrizione al topic in cui vengono pubblicati i campioni ed aggiorna la lista
 * @returns
 */
function subscribe() {
	// Gestione della visibilità dei bottoni quando viene effettuata la connessione
	$('#disconnect').css('display', 'block');
	$('#connect').css('display', 'none');
	if(window.WebSocket) {
		var client;
		var login = '';
		var passcode = '';	
		var url = 'ws://localhost:61614';			// URI del broker ActiveMQ
		var topic = localStorage.getItem("topic");	// Recupero del nome del topic dallo storage locale del browser
		var operation = localStorage.getItem("operation");	// Recupero del nome dell'operazione topic dallo storage locale del browser
		var destination = '/topic/' + operation + topic;		// Costruzione seconda parte dell'URI
		
		// Inserisce il titolo della pagina
		$("#topbar_message").text("Console di Monitoraggio Sensori per il topic " + operation + topic);
		
		initSamplesTableHead(operation, topic);
		
		// Callback per il metodo subscribe, aggiunge elementi alla tabella di monitoraggio quando viene ricevuto un messaggio
		var on_message = function(message) {
			updateSamplesTableBody(message);
		}

		// Callback per il metodo connect, effettua la connessione al broker e la sottoscrizione al topic
		var connect_callback = function(frame) {
			// Visualizza popup informativo
			swal("Connessione effettuata!", "", "success");
			// Effettua la sottoscrizione al topic
			client.subscribe(destination, on_message);
		}

		// Callback per il metodo connect, in caso di errori di connessioni mostra un messaggio a video e nella console del browser
		var error_callback = function(error) {
			swal("Impossibile connettersi al broker MQTT", error, "error")
		}
		
		// Effettua la disconnessione quando viene cliccato il bottone
		$('#disconnect').click(function() {
			disconnect(client);
		});
		
		// Creazione client STOMP su websocket
		client = Stomp.client(url);
		// Connessione del client al broker
		client.connect(login, passcode, connect_callback, error_callback);
	} else {
		// Visualizza un messaggio se il browser non suporta websocket
		swal("Get a new Web Browser!", "Il tuo browser non supporta WebSockets", "error")
	}
}

/**
 * Inserisce l'intestazione della tabella
 * @param operation è l'operazione da inserire nella riga a scopo informativo
 * @param topic è il topic da inserire nella riga a scopo informativo
 * @returns
 */
function initSamplesTableHead(operation, topic) {
	$("#samples_list thead").replaceWith(
			"<tr>" +
				"<th style=\"margin: auto; text-align: center;\">Timestamp</th>" +
				"<th style=\"margin: auto; text-align: center;\">Campioni di " + operation + " " + topic + "</th>" +
			"</tr>");
}

/**
 * Aggiorna la tabella dei campioni inserendo una nuova riga in testa
 * @param message è il messaggio da cui estrarre i campioni
 * @returns
 */
function updateSamplesTableBody(message) {
	$("#samples_list tbody").prepend(	// Con prepend aggiungo in testa alla tabella
			  "<tr>" +
				  "<td>" + timestamp() + "</td>"+				// Timestamp
				  "<td>" + message.headers.sample + "</td>" +	// Campione (temperatura, umidità o luminosità)
			  "</tr>\n");
}

/**
 * Effettua la disconnessione dal broker
 * @param client è il client da disconnettere
 * @returns
 */
function disconnect(client) {
	swal({
		title: "Sei sicuro di volerti disconnettere?",
		text: "disconnettendoti la pagina non verrà chiusa",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			client.disconnect(function() {
				swal("Disconnesso!", {
				    icon: "success",
				});
				// Gestione della visibilità dei bottoni quando viene effettuata la disconnessione
				$('#disconnect').css('display', 'none');
				$('#connect').css('display', 'block');
				//window.close();	// Chiude la finestra
			});
		}
	});
}

/**
 * Recupera e formatta il timestamp nel formato gg/MM/yyy hh:mm:ss
 * @returns
 */
function timestamp() {
	now 	= new Date();
	year 	= "" + now.getFullYear();
	month	= "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	day 	= "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	hour 	= "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	minute 	= "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	second 	= "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	return day + "/" + month + "/" + year + "  " + hour + ":" + minute + ":" + second;
}