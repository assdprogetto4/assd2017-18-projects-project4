package it.unisannio.mqtt.client;

// Questa interfaccia serve da listener per i messaggi arrivati al subscriber (vedi implementazione messageArrived di MqttSubscriberClient) 
public interface MqttMessageListener {
	public void messageReceived(String message);
}