package it.unisannio.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttSubscriberClient extends WSNMqttClient {
	private MqttMessageListener listener;

	/**
	 * Costruttore per subscriber MQTT, il clientId viene generato automaticamente
	 */
	public MqttSubscriberClient() {
		super();
	}
	
	/**
	 * Costruttore per subscriber MQTT
	 * @param clientId e' l'id del subscriber
	 */
	public MqttSubscriberClient(String clientId) {
		super(clientId);
	}

	/**
	 * Costruttore per subscriber MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del subscriber
	 */
	public MqttSubscriberClient(String remoteBrokerIP, String clientId) {
		super(remoteBrokerIP, clientId);
	}

	/**
	 * Costruttore per subscriber MQTT
	 * @param remoteBrokerIP e' l'IP del broker MQTT
	 * @param clientId e' l'id del subscriber
	 * @param QoS e' la qualita' del servizio (0 per politica at most once, 1 per politica at least once, 2 per politica exactly once)
	 */
	public MqttSubscriberClient(String remoteBrokerIP, String clientId, int QoS) throws MqttException {
		super(remoteBrokerIP, clientId, QoS);
	}	

	/**
	 * Wrapper per il metodo publish di MQTT
	 * @param topic e' il nome del topic su cui effettuare la pubblicazione
	 * @param msg e' il messaggio da pubblicare
	 * N.B.: Nel subscriber non mi interessa implementarlo
	 */
	public void publish(String topic, String msg) { }

	/**
	 * Wrapper per il metodo subscribe di MQTT
	 * @param topic e' il nome del topic a cui effettuare la sottoscrizione
	 */
	public void subscribe(String topic) {
		try {
			if(client.isConnected()) {
				// Effettua la sottoscrizione al topic
				System.out.println("Subscribed to the topic: " + topic);
				client.subscribe(topic, QoS);
			}
		} catch (MqttException e) {
			//System.out.println(e.getMessage());
		}
	}

	/**
	 * Questo metodo setta un listener per i messaggi del subscriber
	 * @param listener e' un listener per messaggi MQTT
	 */
	public void setMessageListener(MqttMessageListener listener) {
		this.listener = listener;
	}

	/**
	 * Questa callback e' invocata quando un messaggio di un topic a cui siamo sottoscritti viene ricevuto.
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		if (this.listener != null) {
			this.listener.messageReceived(message.toString());
		}
	}
}