package it.unisannio.sensor.console;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unisannio.aggregator.AggregatorNodeServer;
import model.Sensor;
import service.ConsoleLocal;

@WebServlet("/ManagementConsole")
public class ManagementConsole extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB private ConsoleLocal console;	// Con l'annotazione EJB faccio dependency injection, in questo modo creo una dipendenza tra layer di presenzazione e di applicazione
	int i = 0;

	public ManagementConsole() { super(); }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sensorType = request.getParameter("type");		// Recupera il tipo del sensore
		List<Sensor> sensors = console.getSensors(sensorType);	// Recupera la lista sensori dal database
		String elements = "";
		for(Sensor s : sensors)	elements += s.toHTMLSelectElement();	// Li converte in righe per tabella HTML 

		// Li invia come risposta al client
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(elements);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String subscribeTopic = request.getParameter("topic");	// Recupera il nome del topic a cui sottoscrivere il nodo aggregatore
		String operation = request.getParameter("operation");	// Recupera l'operazione di aggragzaione
		String publishTopic = operation + subscribeTopic;		// Costruisce il nome del topic su cui pubblicare

		int[] ports = new int[] {1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238};
		int port = ports[i++ % ports.length];
		System.out.println("Subscribe Topic: " + subscribeTopic + "\nPublish Topic: " + publishTopic + "\nOperation: " + operation + "\nPort: " + port + "\n");

		// Creo l'aggregatore principale e quello di backup e li avvio
		AggregatorNodeServer aggregator = new AggregatorNodeServer(subscribeTopic, publishTopic, operation, port);
		aggregator.start();		

		// Costruisce la riga della tabella degli aggregatori
		String tableRow =
				"<tr style=\" margin:auto; text-align:center; border: 1px solid black;\">" + 
						"<td style=\" margin:auto; text-align:center; border: 1px solid black;\">" + publishTopic + "</td>" +
						"<td style=\" margin:auto; text-align:center; border: 1px solid black;\"><button type=\"submit\" onclick=\"openMonitor(this)\" class=\"w3-button w3-blue\" style=\"width:100%\">Apri</button></td>" +
						"<td style=\" margin:auto; text-align:center; border: 1px solid black;\">" + operation + "</td>" +
						"<td style=\" margin:auto; text-align:center; border: 1px solid black;\" id=\"sensor_topic\">" + subscribeTopic + "</td>" +
						"</tr>";
		// Costruisce un elemento della lista di topic
		String option = "<option>" + subscribeTopic + "</option>";
		// Li scrive su file
		writeFile("/topic_dropdown_list.txt", option);
		writeFile("/topic_table.txt", tableRow);
		// Restituisce la riga della tabella degli aggregatori come risposta al client
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(tableRow);
	}

	private void writeFile(String fileName, String content) {
		// Recupera il path del file
		String contextPath = getServletContext().getRealPath(fileName);
		try {
			File file = new File(contextPath);
			if(!file.exists()) file.createNewFile();
			List<String> fileLines = Files.readAllLines(file.toPath());

			boolean line_exist = false;
			for(String line : fileLines) if(line.equals(content)) line_exist = true;		
			// Per evitare duplicati controllo se una riga gi� esiste
			if(!line_exist)	
				Files.write(file.toPath(), (content + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
		} catch(Exception e) {e.printStackTrace();}
	}
}
