package it.unisannio.sensor.console;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import service.ConsoleLocal;

@MessageDriven(
		activationConfig = { 
				@ActivationConfigProperty(
						propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
				@ActivationConfigProperty(
						propertyName = "destination", propertyValue = "registration")
		})
public class SensorsChecker implements MessageListener {
	@EJB private ConsoleLocal console;	// Con l'annotazione EJB faccio dependency injection, in questo modo creo una dipendenza tra layer di presenzazione e di applicazione.

	public SensorsChecker() { }

	public void onMessage(Message message) {
		try {
			// NOTA: i sensori inviano un messaggio MQTT, il quale e' binario senza informazioni sul tipo di contenuto o intestazioni per definire il payload.
			// Per questo motivo ActiveMq li interpretera' sempre come BytesMessage e li inviera' come tali. 
			// Percio' e' necessario consumare BytesMessage e quindi leggere il payload come una stringa.
			BytesMessage byteMsg = (BytesMessage) message;
			byte byteMsgContent[];
			byteMsgContent = new byte[(int) byteMsg.getBodyLength()];
			byteMsg.readBytes(byteMsgContent);
			String stringMsgContent = new String(byteMsgContent);
			System.out.println(stringMsgContent);
			// Una volta convertito il messaggio da byte[] a String posso parsarlo come oggetto JSON
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(stringMsgContent);
			String sensorId = (String) json.get("sensorId");
			String sensorType = (String) json.get("type");
			System.out.println("\nRicevuto id: " + sensorId);
			System.out.println("Ricevuto tipo: " + sensorType);
			console.createSensor(sensorId, sensorType);
		} catch(ParseException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
