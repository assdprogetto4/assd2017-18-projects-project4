package service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import model.Sensor;

@Stateless
public class Console implements ConsoleLocal {
	// La variabile EntityManager non e' inizializzata, questo perche' delego l'application server alla creazione dell'oggetto.
	// Sto dicendo inetta in em il gestore della persistenza che e' una istanza di hybernate.
	@PersistenceContext
	EntityManager em;

	public Console() { }

	public void createSensor(String sensorId, String sensorType) {
		try {
			getSensor(sensorId);
		} catch(NoResultException e) {
			Sensor sensor = new Sensor(sensorId, sensorType);
			em.persist(sensor);
		}
	}
	
	public List<Sensor> getSensors(String sensorType) {
		return em.createNamedQuery("findSensorByType", Sensor.class).setParameter("sensorType", sensorType).getResultList();
	}
	
	public Sensor getSensor(String sensorId) throws NoResultException {
		return em.createNamedQuery("findSensor", Sensor.class).setParameter("sensorId", sensorId).getSingleResult();
	}
}
