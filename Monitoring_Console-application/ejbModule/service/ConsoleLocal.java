package service;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.NoResultException;

import model.Sensor;

@Local
public interface ConsoleLocal {	
	public void createSensor(String sensorId, String sensorType);
	public List<Sensor> getSensors(String sensorType);
	public Sensor getSensor(String sensorId) throws NoResultException;
}