package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Sensor
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="findSensorByType", query = "SELECT s FROM Sensor s WHERE s.sensorType = :sensorType"),
	@NamedQuery(name="findSensor", query = "SELECT s FROM Sensor s WHERE s.sensorId = :sensorId")
})
public class Sensor implements Serializable {
	// Chiave gestita dal DBMS automaticamente
	@Id private String sensorId;	// Con l'annotazione @Id sto dicendo che la variabile sensorId e' chiave primaria
	@SuppressWarnings("unused")
	private String sensorType;
	private static final long serialVersionUID = 1L;

	public Sensor() { }
	public Sensor(String sensorId, String sensorType) {
		this.sensorId = sensorId;
		this.sensorType = sensorType;
	}
	public String toHTMLSelectElement() { 
		return "<option value=\"" + sensorId + "\">" + sensorId + "</option>"; 
	}
}
