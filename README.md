**ASSD Challenge - Project 4: Framework per la gestione di una rete di sensori, il controllo di dispositivi di attuazione e il monitoraggio dei dati acquisiti.**

Il progetto prevede la realizzazione di un framework per la gestione di nodi sensore, l’acquisizione di misurazioni prodotte dai sensori, il controllo di dispositivi di attuazione e il monitoraggio dei dati acquisiti. 

---

## Nodi sensore

Ciascun nodo sensore può essere modellato come un componente composto di:

1. un insieme di sensori, in grado di misurare parametri chimico/fisici diversi (es. temperatura, umidità, luminosità, ecc.).
2. un modulo di comunicazione in grado di inviare i dati acquisiti a nodi logici di aggregazione. 

---

## Nodo di aggrezazione

Si assuma di avere un nodo logico di aggregazione dei dati per ogni tipo di sensore/parametro da monitorare ed un nodo analogo di backup da utilizzare in caso di malfunzionamento del nodo di aggregazione attivo. 

---

## Attuatori

Si inviino i dati aggregati ad attuatori in grado di agire sulla base delle misurazioni aggregate ricevute

---

## Monitor

Tutti  i  dati  dovranno  essere  inviati  anche  ad  una  console  di  monitoraggio  Web  based  al  fine  di presentarli in tabelle aggiornate dinamicamente

---